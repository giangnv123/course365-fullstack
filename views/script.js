const ALL_COURSE_URL = `api/v1/courses`;

const popularCourseContainer = $("#course-popular-container");
const trendingCourseContainer = $("#course-trending-container");
const courseLoadNumber = 4;

const trending = "isTrending";
const popular = "isPopular";

$(function () {
  loadCourse(trending, trendingCourseContainer);
  loadCourse(popular, popularCourseContainer);
});

async function loadCourse(condition, container) {
  var vAllCourses = await getAllCourse();

  var vCourseMeetCondition = vAllCourses.filter((course) => course[condition] === true);

  container.empty();

  vCourseMeetCondition.slice(0, courseLoadNumber).forEach((course) => {
    var vCardCourse = getCardCourseHtml(course);
    container.append(vCardCourse);
  });
}

async function getAllCourse() {
  var vResponse = await fetch(ALL_COURSE_URL);
  var vAllCourse = await vResponse.json();
  console.log(vAllCourse);
  return vAllCourse.courses;
}
getAllCourse();

function getCardCourseHtml(course) {
  var vHtml = ` <div class="card-container">
                  <div class="card h-100">
                  <img src="${course.coverImage}" class="card-img" />
                  <div class="card-body">
                      <h5 class="text-primary">
                          ${course.courseName}
                      </h5>
                      <div class="d-flex align-items-center text-secondary my-3">
                      <i class="fas fa-clock fa-lg"></i>
                      <span class="course-time mx-2">${course.duration}</span>
                      <span class="course-level">${course.level}</span>
                      </div>
                      <div class="d-flex align-items-center">
                      <span class="sale-price fw-6 me-2">$${course.discountPrice}</span>
                      <span class="real-price fw-4 text-secondary">$${course.price}</span>
                      </div>
                  </div>
                  <div class="card-footer">
                      <div class="flex-layout">
                      <div class="d-flex align-items-center">
                          <img
                          src="${course.teacherPhoto}"
                          class="img-teacher"
                          />
                          <span class="ms-2 teacher-name">${course.teacherName}</span>
                      </div>
                      <i class="fas fa-bookmark text-secondary"></i>
                      </div>
                  </div>
                  </div>
                  </div>`;
  return vHtml;
}
