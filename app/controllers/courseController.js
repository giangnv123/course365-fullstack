const mongoose = require("mongoose");
const Course = require("../models/courseModel");

// Get All Courses
exports.getCourses = async (req, res) => {
  try {
    let condition = {};
    const userId = req.query.userId;
    if (userId) condition.user = userId;

    const courses = await Course.find(condition);
    res.status(200).json({
      message: "Success",
      results: courses.length,
      courses,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get all Courses!",
      error,
    });
  }
};

// Get Course
exports.getCourse = async (req, res) => {
  try {
    const id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const course = await Course.findById(req.params.id);

    if (!course)
      return res.status(404).json({
        message: "Course not found!",
      });

    res.status(200).json({
      message: "Success",
      course,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get Course!",
      error,
    });
  }
};

// Update Course
exports.updateCourse = async (req, res) => {
  try {
    const id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const course = await Course.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });

    if (!course)
      return res.status(404).json({
        message: "Course not found!",
      });

    res.status(200).json({
      message: "Success",
      course,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to update course.",
      error,
    });
  }
};

// Create Course
exports.createCourse = async (req, res) => {
  try {
    const course = await Course.create(req.body);

    res.status(201).json({
      message: "Success",
      course,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to create Course!",
      error,
    });
  }
};

// Delete Course

exports.deleteCourse = async (req, res) => {
  try {
    const id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const course = await Course.findByIdAndDelete(id);

    if (!course)
      return res.status(404).json({
        message: "Course not found!",
      });

    res.status(204).json({
      message: "Success",
      course,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to delete Course!",
      error,
    });
  }
};
