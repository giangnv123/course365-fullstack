const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const courseSchema = new Schema({
  courseCode: {
    type: String,
    required: [true, "Please Provide drink code"],
    unique: [true, "Existing Course Code"],
  },
  courseName: {
    type: String,
    required: [true, "Course must have a name!"],
  },
  price: {
    type: Number,
    required: [true, "Course must have a price!"],
  },
  discountPrice: {
    type: String,
    required: [true, "Course Must have discount price!"],
  },
  duration: {
    type: String,
    required: [true, "Course must have duration!"],
  },
  level: {
    type: String,
    required: [true, "Course must have a level!"],
  },
  coverImage: {
    type: String,
    required: [true, "Course must have an image!"],
    unique: true,
  },
  teacherName: {
    type: String,
    required: [true, "Course must have teacher name!"],
  },
  teacherPhoto: {
    type: String,
    required: [true, "Course must have teacher's photo"],
  },
  isPopular: {
    type: Boolean,
    default: true,
  },
  isTrending: {
    type: Boolean,
    default: false,
  },
});

const Course = mongoose.model("Course", courseSchema);

module.exports = Course;
