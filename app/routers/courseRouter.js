const express = require("express");

const { getCourses, getCourse, createCourse, updateCourse, deleteCourse } = require("../controllers/courseController");

/***** ROUTER *****/

// Create router
const courseRouter = express.Router();

courseRouter.route("/").get(getCourses).post(createCourse);

courseRouter.route("/:id").patch(updateCourse).get(getCourse).delete(deleteCourse);

// Export course Router
module.exports = { courseRouter };
