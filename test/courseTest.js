const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");
const expect = require("chai").expect;
const should = chai.should();
const mongoose = require("mongoose");
const mongoUrl = `mongodb://127.0.0.1:27017/CRUD_Course-test`;
const Course = require("../app/models/courseModel");

chai.use(chaiHttp);

describe("Test Course API", () => {
  it("should connect and disconnect to mongodb", async () => {
    // console.log(mongoose.connection.states);
    mongoose.disconnect();
    mongoose.connection.on("disconnected", () => {
      expect(mongoose.connection.readyState).to.equal(0);
    });
    mongoose.connection.on("connected", () => {
      expect(mongoose.connection.readyState).to.equal(1);
    });
    mongoose.connection.on("error", () => {
      expect(mongoose.connection.readyState).to.equal(99);
    });

    await mongoose.connect(mongoUrl);
  });

  // Create Course
  describe("Create Course (POST)", () => {
    it("should create a new course", async () => {
      await Course.deleteMany();

      const newCourse = {
        courseCode: "C-1789",
        courseName: "Basic CSSB",
        price: 499,
        discountPrice: 400,
        duration: "60 hours",
        level: "Beginner",
        coverImage: "https://media.bgeeksforgeeks.org/wp-content/cdn-uploads/20220428152156/CSS-Cheat-Sheet-4.jpg",
        teacherName: "Lucy Hale",
        teacherPhoto:
          "https://images.unsplash.com/photo-1605711285791-0219e80e43a3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2069&q=80",
        isPopular: true,
        isTrending: true,
      };

      const res = await chai.request(server).post("/api/v1/courses").send(newCourse);

      res.should.have.status(201);
      res.body.course.should.be.a("object");
      res.body.course.should.have.property("courseCode", "C-1789");

      // Add any additional assertions you want to make about the created course object
    });
  });

  // Get ALl Course
  describe("Get All Courses", () => {
    it("Should get All Courses", async () => {
      const res = await chai.request(server).get("/api/v1/courses");

      res.should.have.status(200);
      res.body.courses.should.be.an("array");
    });
  });

  // Get Course By Id
  describe("Get Course By Id (Get)", () => {
    it("Should Get A Course By Id", async () => {
      const newCourse = {
        courseCode: "C-109",
        courseName: "Basic CSSA",
        price: 499,
        discountPrice: 400,
        duration: "60 hours",
        level: "Beginner",
        coverImage: "https://media.geekasforgeeks.org/wp-content/cdn-uploads/20220428152156/CSS-Cheat-Sheet-4.jpg",
        teacherName: "Lucy Hale",
        teacherPhoto:
          "https://images.unsplash.com/photo-1605711285791-0219e80e43a3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2069&q=80",
        isPopular: true,
        isTrending: true,
      };
      // Create a course

      const createdCourse = await chai.request(server).post("/api/v1/courses").send(newCourse);

      // Get Course by id
      const courseId = createdCourse.body.course._id;

      const res = await chai.request(server).get(`/api/v1/courses/${courseId}`);

      // Test
      res.should.have.status(200);
      res.body.course.should.be.a("object");
      res.body.course.should.have.property("courseCode", "C-109");
    });
  });

  // Update Course By Id
  describe("Update Course By Id (PUT)", () => {
    it("should update a course by id", async () => {
      const newCourse = {
        courseCode: "C-1099",
        courseName: "Basic CSSaaa",
        price: 499,
        discountPrice: 400,
        duration: "60 hours",
        level: "Beginner",
        coverImage: "https://media.geeksdfaargeeks.org/wp-content/cdn-uploads/20220428152156/CSS-Cheat-Sheet-4.jpg",
        teacherName: "Lucy Hale",
        teacherPhoto:
          "https://images.unsplash.com/photo-1605711285791-0219e80e43a3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2069&q=80",
        isPopular: true,
        isTrending: true,
      };
      const createdCourse = await chai.request(server).post("/api/v1/courses").send(newCourse);

      console.log(createdCourse.body, "created Course body");
      const courseId = createdCourse.body.course._id;

      // Update course with new values
      const updatedCourse = {
        courseCode: "C-106",
      };
      const res = await chai.request(server).patch(`/api/v1/courses/${courseId}`).send(updatedCourse);

      res.should.have.status(200);
      res.body.course.should.have.property("courseCode", "C-106");

      // Additional assertions if needed
    });
  });

  // Delete Course By Id
  describe("Delete Course By Id (DELETE)", () => {
    it("should delete a course by id", async () => {
      // First, create a course to delete
      const newCourse = {
        courseCode: "C-997",
        courseName: "Basic CSSaee",
        price: 499,
        discountPrice: 400,
        duration: "60 hours",
        level: "Beginner",
        coverImage: "https://media.geeksforgeeks.org/wp-content/cdn-uploads/20220428152156/CSS-Cheat-Sheet-4.jpg",
        teacherName: "Lucy Hale",
        teacherPhoto:
          "https://images.unsplash.com/photo-1605711285791-0219e80e43a3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2069&q=80",
        isPopular: true,
        isTrending: true,
      };
      const createdCourse = await chai.request(server).post(`/api/v1/courses`).send(newCourse);

      const courseId = createdCourse.body.course._id;

      // Delete course
      const res = await chai.request(server).delete(`/api/v1/courses/${courseId}`);

      res.should.have.status(204);
    });
  });
});
