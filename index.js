const mongoose = require("mongoose");
const express = require("express");

// Get Routers
const { courseRouter } = require("./app/routers/courseRouter");

const app = express();
const port = 8000;

app.use(express.json());

// View Course Page
app.use(express.static(__dirname + "/views"));

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname + "/views/index.html"));
});

// Connect to mongoDb Local Database
const mongoUrl = `mongodb://127.0.0.1:27017/CRUD_Course-test`;
mongoose.connect(mongoUrl).then(() => console.log(`DB Connection to Local Database Successfully!`));

app.use("/api/v1/courses", courseRouter);

app.listen(port, () => console.log(`App is running on port: ${port}`));

module.exports = app;
